package lesson3;

import java.util.Arrays;

public class ArrMaxMinOutput {
    public static void main(String[] args) {
        int[] arr = {2, 7, 77, 0, 13};
        int[] arr2 = arr.clone();
        Arrays.sort(arr);
        System.out.println("minimal: " + arr[0]);
        System.out.println("max: " + arr[arr.length - 1]);
        System.out.println("Array :" + Arrays.toString(arr));
    }
}
