package Lesson2;

import java.util.Scanner;

public class LastNumber7 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Input number");
        int number = sc.nextInt();
        if (number%10 ==7) System.out.println("This number last digit is 7");
    }
}
