package Lesson2;

import java.util.Scanner;

public class TimeInSec {
    public static void main(String[] args) {
        System.out.println("Input time in sec");
        Scanner cs = new Scanner(System.in);
        int sec = cs.nextInt();

        int hours = sec/3600;
        int min = (sec-hours*3600)/60;
        int lastSec = sec-hours*3600-min*60;
        System.out.println("Time: "+hours+" : "+min+" : "+lastSec);
    }
}
